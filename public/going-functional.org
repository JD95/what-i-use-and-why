#+title: Going Functional 
#+startup: hidestar
#+startup: indent

# latex options
#+options: html-postamble:nil date:nil
#+latex_header: \usepackage[margin=1.5in]{geometry}
#+latex_header: \usepackage{setspace}

* Roadmap
** PROGRESSING Getting Started [0/3]

The first chapter will cover the steps to quickly start writing Haskell code.

This chapter will cover:
- [ ] Installing Stack
- [ ] Creating a project
- [ ] Running ghci
   
** PROGRESSING First Steps [5/6]

Now that the tools needed to write and run Haskell have been setup, we can cover some of the bare essentials to actually make things happen in our programs.

- [ ] What *is* programming?
- [X] Loading file into ghci 
- [X] Basic types and constants
- [X] Simple functions
  + [X] (+), (-), (*), (/)
  + [X] (==), (/=)
  + [X] (<), (>), (<=), (>=)
  + [X] (++)
- [X] First Typeclass
  + [X] Show
- [X] Displaying Messages 
  + [X] putStr
  + [X] putstrln 
  + [X] print

** PROGRESSING Fundamentals [2/4]

This chapter will elaborate on the concepts covered in the previous chapter and cover most of the foundational concepts and functions that will be used to construct most of the other concepts in the book. One should expect to be able to write simple console projects with the materials covered here. 

- [X] Making New Types
  + [X] newtype
  + [X] products and tuples
  + [X] sum types
- [-] Function Building Blocks
  + [X] Case Expressions
  + [X] Pattern Matching
  + [ ] (.) 
- [ ] Common Typeclasses
  + [ ] Num
  + [ ] Eq
  + [ ] Ord
  + [ ] Read
- [X] IO Pipes and Getting User Input
  + [X] getLine
  + [X] (>>=)

** PROGRESSING Simple Abstractions [0/8]

Code reusability is a valued asset as projects become larger and more complicated. This chapter will introduce some of the tools that help Haskell programmers do more with less. While this chapter only scratches the surface with the abstraction potential in Haskell code, it should be enough for most needs early on. 

- [ ] Viewing Types In ghci
- [ ] Generic Types
- [ ] Maybe a 
- [ ] [a]
  + unfoldr
  + map
  + fold
- [ ] NonEmpty a
- [ ] When to make a type generic?
- [ ] Making Typeclasses
  + [ ] When to make a Typeclass?
- [ ] Working with Files
  + [ ] readFile
  + [ ] writeFile
  + [ ] appendFile

** PROGRESSING Using Data for Control [0/5]

By now we should have covered enough material to feel comfortable collecting and transforming data. This chapter covers the various ways that transformations can be made more robust be introducing different ways to control the flow of the program.

- [ ] Control Flow with Bool Values
  + [ ] if
  + [ ] function guards 
- [ ] Control Flow with Maybe
- [ ] Control Flow with Either
- [ ] Control Flow with Lists
- [ ] Functor Pipes

** TODO Working with State [0/2]

Sometimes functions need to work with extra information beyond the normal kinds of transformations which have been covered so far. This chapter introduces the =Reader= type, which helps pass around useful values that do not change, like configurations, and =State= which helps manage changing values like a board in a game. 

- [ ] Reader
- [ ] State

** TODO Object Oriented Programming [0/2]

This chapter isn't necessary to be productive in Haskell, infact, I advise anyone learning Haskell as a first language to skip this chapter. However, if you have previous experience in object oriented languages this chapter might give a new perspective on what objects can be and see how OO design patterns might transcribe into a functional setting.

 1. [ ] An Object in Haskell
 2. [ ] Translating Design Patterns

** TODO Data Parsing [0/3]

Programming doesn't happen in a vacuum. There are many established formats and practices which help programs, and programmers, interact even across platforms. More over, we shouldn't be expected to have to write everything from scratch all the time. This chapter covers how to find and install libraries using =stack= to help us make use of solutions other people have come up with to our problems. It will also cover how to deal with two common data formats, JSON and CSV along with their corresponding Haskell libraries Aeson and Cassava.

- [ ] Using libraries with stack
- [ ] JSON with Aeson
- [ ] CSV with Cassava

** TODO Common Data Structures [0/3]

Although one can go far with simple lists, they are not our only choice and often not the best choice when working with data. This section will cover different container types and some of the theory that goes behind them.

1. [ ] Vector
2. [ ] Sequence
3. [ ] Map

** TODO String, Text, and ByteString [0/4]

Haskell, for all its glory, does have a few warts to deal with. One of the annoyances in Haskell is the variety of ways to represent strings and the frequency with which one has to juggle these various representations. This chapter will look over the various string libraries, clarify when to use which, and suggest some tools for making string usage in Haskell easier.

1. [ ] String
2. [ ] Text
3. [ ] ByteString
4. [ ] StringConv

** TODO Building a Ledger [0/3]

With the materials from previous chapters we should have enough Haskell under our belts to try and build something useful. This chapter will follow the process from start to finish of building a command line ledger program to help track spending. We'll cover building an executable using =stack= as well as introduce some typeclasses that naturally arise when programs start to get larger, =Applicative= and =Monad=.

- [ ] Building an executable with stack
- [ ] Applicative Parsers
  + [ ] optparse-applicative
  + [ ] alternative
- [ ] Monadic Parsers
  + [ ] megaparsec

** TODO Writing Correct Code [0/4]

As projects get larger, it becomes more important to ensure that the code written does it we think it does. There are several tools covered in this chapter that help Haskell programmers check and ensure the correctness of their code.

1. [ ] QuickCheck
2. [ ] HSpec
3. [ ] Kinds
4. [ ] GADTs and Existental Types

** TODO Record Access and Updates [0/4]

Updating values in Haskell can be a pain. Records don't really make things easier and at times, they make things more awkward than necessary. This chapter will build up to what is currently the standard for updating complex structures in Haskell. The cousins of the =Functor= typeclass will be covered.

1. [ ] Bifunctor
2. [ ] Contravariant
3. [ ] Profunctor
4. [ ] Lens

** TODO Monad Transformers [0/5]

Although we briefly covered the =Monad= typeclass in previous chapters, we'll go more in depth into how the instances of =Monad= are used in typical Haskell applications.

1. [ ] mtl
2. [ ] MonadIO
3. [ ] MonadState
4. [ ] MonadReader
5. [ ] mtl style classes

** TODO Logging [0/1]

How to record events throughout a Haskell program.
- [ ] What logging library to use

** TODO Exceptions [0/2]

Although most Haskell code tries to make the potential for failure explicit in the type system, sometimes there are cases where values cannot be calculated and an exception is thrown.

1. [ ] How to handle exceptions?
2. [ ] MonadCatch vs Either

** TODO GHC Generics [0/1]

This chapter goes through the machinery needed to make generic derivings for typeclasses, helping reduce boilerplate code.

1. [ ] Deriving generic instances

** TODO Databases [0/1]

When the data used by a program needs to persist after the program end or there is a lot of data that needs to be queried in different ways, simply reading and writing data to a file isn't enough. Like most other languages, there are ways for Haskell programs to interact with databases. This chapter will walk through using several database libraries with different approaches.

1. [ ] Persistent

** TODO Parallelism [0/2]

One of the many benefits of using Haskell is that it's immutability and expression based programs make running multiple tasks at once simpler. This chapter will go over the various methods of speeding up programs by applying parallelism.  

- [ ] parMap
- [ ] deepSeq

** TODO Concurrency [0/4]

While parallel programming deals with tasks that run independently of eachother, concurrency deals with tasks that communicate and interact. This chapter goes through the various tools that are available in Haskell to deal with concurrency.

1. [ ] MVar
2. [ ] TVar
3. [ ] STM
4. [ ] Chan

** TODO Networking [0/4]

This chapter will give a tour of network programming in Haskell including interacting with web APIs and websockets, utilizing some concurrency from the previous chapter to create a small chat server, and finally we'll look at creating a simple http server with the library Warp.

1. [ ] Http Requests
2. [ ] Websockets
3. [ ] Tiny Chat Server 
4. [ ] Warp

** TODO HTTP Server [0/5]

This chapter will walk through building a webserver in Haskell. The server will have several security features, a database, and generate html webpages.

1. [ ] Servant
2. [ ] BlazeHtml
3. [ ] Logging
4. [ ] Adding a Database
5. [ ] Authentication and Encryption

** TODO Writing Efficent Code [0/7]

Sometimes your Haskell code simply wont run fast enough. Although Haskell is not a langauge designed to maximize speed and efficency, there are some things we can do to help speed up our code when we need to.

1. [ ] Profiling Code
2. [ ] Criterion
3. [ ] Strictness
4. [ ] Unboxed data
5. [ ] ST
6. [ ] Regions
7. [ ] Garbage Collection

** TODO Purescript [0/3]

- [ ] Setup
- [ ] Hello World
- [ ] Differences from Haskell

** TODO Nix [0/4]
1. [ ] What is Nix
2. [ ] Package installation
3. [ ] Derivations
4. [ ] Overrides
** PROGRESSING Church Encodings [0/4]

A Church encoding is a means of representing data using functions alone. Everything from Bool and Int values to ADTs can be implemented using functions. *Just* fuctions. While knowing how to do this isn't necessary to use Haskell most of the time, it is an enlightening perspective to take on just how minimal the foundations of a language can be. This chapter will take the building blocks from the earlier chapters and break them down into functional atoms.

1. [ ] RankNTypes
2. [ ] Lists as folds
3. [ ] The (->) Monad instance
4. [ ] SKI Combinators

** PROGRESSING Comonads [0/7]

While not as popular as its counterpart typeclass =Monad=, the =Comonad= type class and its instances can prove to be invaluable in several cases. 

1. [ ] Extract
2. [ ] Extend
3. [ ] Env
4. [ ] Store
5. [ ] kfix
6. [ ] Stream
7. [ ] ComonadSheet

** TODO Free and Cofree Monads [0/2]
1. [ ] Free
2. [ ] Cofree

** TODO Recursion Schemes [0/5]
1. [ ] Catamorphisms
2. [ ] Anamorphism
3. [ ] Prepomorphism
4. [ ] Apomorphism
5. [ ] Hylomorphism

** TODO Type Level Programming [0/4]
1. [ ] Kinds as values
   + [ ] GHC.TypeLits
   + [ ] Data.Proxy
2. [ ] Type Families as Functions
   + [ ] Injectivity
3. [ ] Type Classes as Pattern Matching
   + [ ] Handling Overlapping instances
4. [ ] Final Tagless Embedded Languages

** TODO Trees that Grow [0/2]

This chapter covers the techniques from the paper "Trees that Grow"

- [ ] Extensible Records 
- [ ] Extensible Sum types 
** TODO Functional Reactive Programming [0/1]
- [ ] Reflex 

* Getting Started 
#+INCLUDE: "going-functional/getting-started.org"
* First Steps
#+INCLUDE: "going-functional/first-steps.org"
* Fundamentals
#+INCLUDE: "going-functional/fundamentals.org"
* Simple Abstractions 
#+INCLUDE: "going-functional/simple-abstractions.org"
* Using Data for Control
#+INCLUDE: "going-functional/using-data-for-control.org"
* Objects 
#+INCLUDE: "going-functional/objects.org"
* Comonads
#+INCLUDE: "going-functional/comonads.org"
* Church Encodings
#+INCLUDE: "going-functional/church-encodings.org"
* Recursion Schemes
#+INCLUDE: "going-functional/recursion-schemes.org"
* Trees That Grow
#+INCLUDE: "going-functional/trees-that-grow.org"
* Nix
#+INCLUDE: "going-functional/nix.org"
* FRP
#+INCLUDE: "going-functional/frp.org"
* Haskell Quick Start
#+INCLUDE: "going-functional/haskell-quick-start.org"
