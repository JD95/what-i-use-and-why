#+title: Using Emacs
#+startup: hidestar
#+startup: indent

# latex options
#+options: toc:nil num:nil html-postamble:nil date:nil
#+latex_header: \usepackage[margin=1.5in]{geometry}
#+latex_header: \usepackage{setspace}

This following assumes you've set up emacs using the process described in [[./installing-emacs.org][Installing Emacs]].

The tutorial will give small tables for bindings, but all tables will be repeated at the bottom for ease of reference. 

Modifier Key Abbreviations:
| Key          | Abbreviation |
|--------------+--------------|
| alt          | M            |
| ctrl         | C            |
| escape       | ESC|
| enter/return | RET       |

Throughout this guide I will abbreviate key combinations like this:

| action                                 | example                                | abbreviation |
|----------------------------------------+----------------------------------------+--------------|
| key press with no modifiers            | press i                                | i            |
| key press then key press               | press f then s                         | f s          |
| key press with modifiers               | hold control and press f               | C-f          |
| key press with modifier then key press | hold control and press f, then press s | C-f s        |

* Page Navigation

Using emacs with my setup will be different from using other text editors. The most glaring different is that typing text is not actually the default. Because *everything* is done from the keyboard, there is a specific mode setup for inserting text.

When open emacs it will begin on a "scratch" page. Essentially a blank file. To begin typing press =i= to begin typing. Once =i= is pressed, all the normal letter and number keys will behave like normal. When you wish to stop typing and move around you can press either escape (=esc=) or use the =C-[= combo.

* Opening, Creating, and Closing a file

To open a file with emacs press =C-x C-f=. This will open up a menu which will let you either type out the path to the file you want to open, or select it using the =C-n= (move down) and =C-p= (move up) keys. The =C-n= and =C-p= pairings to move up and down works in many situations. These are the default keys for upward and downward navigation, but I swap them out for =j= and =k= in as many places as possible.

[[./images/using-emacs/find-files-example.png]]

In order to create a new file, simply attempt to open a file which does not yet exist.

When you're done with a file, you can close it by typing =C-x k=. This will "kill" the file.

* Saving a file

Once you've made updates to a file that you want to save, simply return to normal mode with =C-[= and type =: w RET=. This will write the file from emacs to your hard drive. If you want to save to another file, usually called "save as" in other programs, return to normal mode and type =: w= as before, but type the name of the file before pressing =RET=. 

For example:
#+BEGIN_SRC bash
: w hello.txt RET
#+END_SRC

The above will save the contents of the opened file to another file named "hello.txt".

* Dired

For more complicated actions, like deleting, renaming, or moving files around we'll use a tool called Dired (short for Directory Edit).

[[./images/using-emacs/dired-example.png]]

To open up the Dired tool type =C-x d= and type in the path for the directory that you want to open. This will work exactly like selecting a file to open or create.

[[./images/using-emacs/selecting-dired-path.png]]

Once Dired is opened you will see a list of all files and folders in that directory. When working with Dired you'll have to enter =insert mode= in order to use the key bindings for it. Just press =i= and the following commands will work. 

| action                 | binding | function                 |
|------------------------+---------+--------------------------|
| move up a line         | p       | dired-previous-line      |
| move down a line       | n       | dired-next-line          |
| rename / move file     | R       | dired-do-rename          |
| mark file for deletion | d       | dired-flag-file-deletion |
| delete marked files    | x       | dired-do-flagged-delete  |

Moving and renaming a file are the same action. Moving the file is simply renaming the file, but also changing the parent folder rather than just the name.

A more extensive guide for using Dired can be found [[https://www.gnu.org/software/emacs/manual/html_node/emacs/Dired.html][here]].

* Multiple Frames

It is possible to use emacs to view and edit multiple files at the same time.

| action                    | binding |
|---------------------------+---------|
| split window vertically   | C-x 2   |
| split window horizontally | C-x 3   |
| close current window      | C-x 0   |
| close other windows       | C-x 1   |

You can swap which window to work in by prefixing normal cursor movement with a =C-w=.

| action                    | bindings |
|---------------------------+----------|
| switch to window above    | C-k      |
| switch to window below    | C-j      |
| switch to window on left  | C-h      |
| switch to window on right | C-l      |
| cycle windows             | C-x o    |

In some situations, the normal keybindings to switch windows will not work (a quirk of evil mode not being universal). In such cases you can use =C-x o= to change out of the current window into the next in the "cycle". Typically this will only need to be done once to get to a buffer in which the normal evil commands will work.

* Basic Org Mode 

- Org mode is an all around writing and organizational tool
- Built on a few building blocks
  1. Headers
  2. Todos
  3. Tags
  4. Times

* Bindings Reference

** Evil Modes

| action                    | binding | function           |
|---------------------------+---------+--------------------|
| navigation / switch modes | C-[     | evil-normal-state  |
| insert text               | i       | evil-insert-state  |
| write over existing text  | R       | evil-replace-state |
| select text               | v       | evil-visual-state  |
| select blocks of text     | C-v     | evil-visual-blocks |

** Normal Mode

Basic Cursor Motion:

| action            | binding | function           |
|-------------------+---------+--------------------|
| move cursor up    | k       | evil-previous-line |
| move cursor down  | j       | evil-next-line     |
| move cursor left  | h       | evil-backward-char |
| move cursor right | l       | evil-forward-char  |

Page Navigation:

| action         | binding | function                   |
|----------------+---------+----------------------------|
| page back      | C-b     | evil-scroll-page-up        |
| page forward   | C-f     | evil-scroll-page-down      |
| top of page    | g g     | evil-goto-first-line       |
| bottom of page | G       | evil-goto-line             |
| center cursor  | z z     | evil-scroll-line-to-center |
| bottom cursor  | z b     | evil-scroll-line-to-bottom |
| top cursor     | z t     | evil-scroll-line-to-top    |

Searching:

| action                        | binding       | function                |
|-------------------------------+---------------+-------------------------|
| search forward for  character | f <character> | evil-find-char          |
| search backward for character | F <character> | evil-find-char-backward |
| search forward                | /             | evil-search-forward     |
| next search result            | n             | evil-search-next        |
| previous search result        | N             | evil-search-previous    |

Replace Text:

| action               | binding                  | function |
|----------------------+--------------------------+----------|
| replace text in line | :s/<text>/<replacement>  |          |
| replace text in file | :%s/<text>/<replacement> |          |

** File Commands

Basic:

| action            | binding | function        |
|-------------------+---------+-----------------|
| save file         | :w      | save-buffer     |
| open/ create file | C-x C-f | helm-find-files |

Dired:

| action                 | binding | function                 |
|------------------------+---------+--------------------------|
| open dired buffer      | C-x d   | dired                    |
| move up a line         | p       | dired-previous-line      |
| move down a line       | n       | dired-next-line          |
| rename / move file     | R       | dired-do-rename          |
| mark file for deletion | d       | dired-flag-file-deletion |
| delete marked files    | x       | dired-do-flagged-delete  |

Note: You must be in =insert mode= for the commands to work
** Frame Commands 
