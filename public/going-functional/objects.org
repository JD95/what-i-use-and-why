Version 1.

Initial formulation with existential quantification to hide the "private" part of the class, via GADTs.

#+BEGIN_SRC haskell
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE TypeApplications #-}

data Object f where
  New :: f a => a -> Object f

type Method st output = st -> (st, output)

class Unit a where
  getHP :: Method a Int
  damage :: Int -> Method a ()

(!) :: Object o -> (forall a. o a => Method a output) -> (Object o, output)
(New s) ! f = let (s', result) = f s in (New s', result)

(#) :: Object o -> (forall a. o a => Method a output) -> output
o # f = snd $ o ! f

(&) :: Object o -> (forall a. o a => Method a ()) -> Object o 
(New s) & f = let (s',_) = f s in New s'

instance Unit Int where
  getHP self = (self, self)
  damage dmg self = (self - dmg, ())

test :: IO ()
test = do
  let orc = New @Unit (5 :: Int)
  print (orc # getHP)
  print (orc & damage 3 # getHP)
#+END_SRC

Version 2.

Changing methods to be executed with State.

#+BEGIN_SRC haskell
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE TypeApplications #-}

module Objects where

import Control.Arrow
import Control.Monad.State.Strict

data Object f where
  New :: f a => a -> Object f

type Method st output = State st output 

class Unit a where
  getHP :: Method a Int
  damage :: Int -> Method a ()

(!) :: Object o -> (forall a. o a => Method a output) -> (output, Object o)
(New s) ! f = second New (runState f s)

(#) :: Object o -> (forall a. o a => Method a output) -> output
(New s) # f = evalState f s

(&) :: Object o -> (forall a. o a => Method a ()) -> Object o 
(New s) & f = New (execState f s)

instance Unit Int where
  getHP = do
    hp <- get
    return hp 
  damage dmg = do
    modify (\hp -> hp - dmg)

test :: IO ()
test = do
  let orc = New @Unit (5 :: Int)
  print (orc # getHP)
  print (orc & damage 3 # getHP)
#+END_SRC

Version 3.

Generalize to StateT to allow for more side effects.

#+BEGIN_SRC haskell
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE TypeApplications #-}

module Objects where

import Control.Arrow
import Control.Monad.State.Strict

data Object f where
  New :: f a => a -> Object f

type Method st m output = StateT st m output 

class Unit a where
  getHP :: MonadIO m => Method a m Int
  damage :: MonadIO m => Int -> Method a m ()

(!) :: Monad m => Object o -> (forall a. o a => Method a m output) -> m (output, Object o)
(New s) ! f = fmap (second New) (runStateT f s)

(#) :: Monad m => Object o -> (forall a. o a => Method a m output) -> m output
(New s) # f = evalStateT f s

(&) :: Monad m => Object o -> (forall a. o a => Method a m ()) -> m (Object o)
(New s) & f = New <$> execStateT f s

instance Unit Int where
  getHP = do
    liftIO $ putStrLn "We're grabbing the HP now!"
    get
  damage dmg = do
    liftIO . putStrLn $ "Unit damaged by " <> show dmg
    modify (\hp -> hp - dmg)

test :: IO ()
test = do
  let orc = New @Unit (5 :: Int)
  print =<< orc # getHP
  print =<< orc # (damage 3 *> damage 1 *> getHP)
#+END_SRC

Version 4.

Generalize to MonadState

Version 5.

Add indirection around the "this" hidden state to make use more ergonomic
