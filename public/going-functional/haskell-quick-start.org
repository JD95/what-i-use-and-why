1. Mutability
2. Conditionals
3. Arrays
4. Loops
5. Null References
6. Doing without Mutability

   
- Begin with IO, do notation, and mutability

Our goal is to illuminate the "pure" aspect of programs that is often ignored in the imperative paradigm. A series of steps and jumps in a file seems to be all there is to programming. Variables dominate our attention, blinding us to the fact they aren't necessary at all! 

A world without variables might, at first glance, seem impossible if not highly restrictive and clumbersome. This is true in languages which have only ever captiulated to the concept of change as changing values, but popularity is not a good metric for truth. With only a few new tools the absence of variables becomes liberating. Without variables we can develop techniques that even the most devout imperative languages now attempt to emulate.

* What is Mutability?

The story of mutability begins with a variable and the means by which we interact with them. There are three components to a varaible, creating a new place to hold values, reading the value stored in it, and changing that value. Mutability, the changing of a value held in a variable, is what we intend to do without. What exactly this means for how we program will be explored later, but first it is important to understand how these changes work. 

#+begin_src python 
def main():
  # Create a new variable and initalize it with 0
  x = 0
  # Update the value by adding 1
  x += 1
  # Update the value by adding 2
  x += 2
  # Extract the value of x, convert to string and print
  print(str(x))
#+end_src

Explain:
  1. lvalues
  2. rvalues

#+begin_src haskell
import Data.IORef

example :: IO ()
example = do
  -- Create a new variable and initalize it with 0
  x <- newIORef 0
  -- Update the value by adding 1
  modifyIORef x (1 +)
  -- Update the value by adding 2
  modifyIORef x (2 +)
  -- Extract the value of x, convert to string and print
  result <- readIORef x
  print result
#+end_src

Explain:
  1. The IO Type
  2. The IORef Type
  3. The newIORef function
  4. Do Notation
     1. The =<-= pulls a pure value out of an =IO= value
  5. The modifyIORef function
  6. The readIORef function
  7. print already converts values into a string first

#+begin_src haskell
import Data.IORef

example :: IO ()
example = do
  -- Create a new variable and initalize it with 0
  x <- newIORef 0
  -- Update the value by adding 1
  x' <- readIORef x
  writeIORef x (1 + x')
  -- Update the value by adding 2
  x'' <- readIORef x
  writeIORef x (2 + x'')
  -- Extract the value of x, convert to string and print
  x''' <- readIORef x
  print x''' 
#+end_src

Explain:
  1. The writeIORef function
     1. writeIORef turns =x= into an lvalue
     2. readIORef turns =x= into an rvalue

#+begin_src haskell
import Data.IORef

example :: IO ()
example = do
  -- Create a new variable and initalize it with 0
  x <- newIORef 0
  -- Update the value by adding 1
  writeIORef x . (1 +) =<< readIORef x
  -- Update the value by adding 2
  writeIORef x . (2 +) =<< readIORef x
  -- Extract the value of x, convert to string and print
  print =<< readIORef x
#+end_src

Explain:
  1. The ==<<= extracts a pure value from =IO= and passes to a function that uses =IO=
  2. The =.= function composes functions

Of course, mutability isn't needed at all here. Without breaking the spirit of starting with a 0, then incrementally adding to it, we can implement the "same" behavior as above without the =IORef= values.

#+begin_src haskell
example :: IO ()
example = do
  print (((2 +) . (1 +)) 0)
#+end_src

We begin with an inital value of =0=, then we perform =1+= and get a new value, and then =2+= to get the final result to print.

#+begin_src python
def example():
  x = 0
  x = str(x)
  x.append("10")
  x = x[1]
  print(x)
#+end_src

#+begin_src haskell
example :: IO ()
example = do
  print (((!! 1) . (<> "10") . show) 0)
#+end_src

As you can see, chains of mutations on a variable are equivalent to composed functions.

* Conditionals
  
One of the differences from other languages is that if statements in Haskell must always include the else branch.

#+BEGIN_SRC cpp
#include <iostream>
#include <string>

int main() {
  std::string input;
  std::cin >> input;
  if (input == "hello") {
    std::cout << "World!\n";
  }
  else {
    std::cout << "Rude!\n";
  }
  return 0;
}
#+END_SRC

#+BEGIN_SRC haskell
import Control.Monad

main = do
  input <- getLine
  if (input == "hello")
    then print "World!"
    else print "Rude!"
#+END_SRC

Else if chains are the same except there must always be an ending else branch. However, if the branching only performs some kind of side effect, then you can use the =when= function to emulate the single if statement.

#+BEGIN_SRC cpp
#include <iostream>
#include <string>

int main() {
  std::string input;
  std::cin >> input;
  if (input == "hello") {
    std::cout << "World!\n";
  }
  return 0;
}
#+END_SRC

#+BEGIN_SRC haskell
import Control.Monad

main = do
  input <- getLine
  when (input == "hello") $ do
    print "World!"
#+END_SRC

* Imperative Loops

Implementing a while loop

#+begin_src haskell
while :: IO Bool -> IO () -> IO ()
while cond body = do
  result <- cond
  if result
    then do
      body
      while cond body
    else return ()
#+end_src

* Arrays

Arrays are not provided by default in Haskell, although their behavior can be imitated using the default list type, it's much better to use the =vector= library if you want an actual container of data.

#+BEGIN_SRC python
def main():
  nums = range(0,10)
  nums.append(5)
  nums.extend(range(20,30))
  for n in nums:
    print n
  print(nums[3])
  for n in nums[4:10]:
    print n
#+END_SRC

#+BEGIN_SRC haskell
import Data.Vector

main = do
  let nums = (fromList [0..9] `snoc` 5) <> fromList [20..29]
  forM_ nums print
  print (nums ! 3)
  forM_ (slice 4 10 nums) print
#+END_SRC

More vector functions can be found here:
https://hackage.haskell.org/package/vector-0.12.0.2/docs/Data-Vector.html

* Translating Tic Tac Toe

Code from [[https://inventwithpython.com/chapter10.html][this site]]

#+begin_src python
import random

def drawBoard(board):

    # This function prints out the board that it was passed.
    # "board" is a list of 10 strings representing the board (ignore index 0)

    print('   |   |')
    print(' ' + board[7] + ' | ' + board[8] + ' | ' + board[9])
    print('   |   |')
    print('-----------')
    print('   |   |')
    print(' ' + board[4] + ' | ' + board[5] + ' | ' + board[6])
    print('   |   |')
    print('-----------')
    print('   |   |')
    print(' ' + board[1] + ' | ' + board[2] + ' | ' + board[3])
    print('   |   |')

def inputPlayerLetter():

    # Lets the player type which letter they want to be.
    # Returns a list with the player’s letter as the first item, and the computer's letter as the second.
    letter = ''

    while not (letter == 'X' or letter == 'O'):
        print('Do you want to be X or O?')
        letter = input().upper()

    # the first element in the list is the player’s letter, the second is the computer's letter.
    if letter == 'X':
        return ['X', 'O']
    else:
        return ['O', 'X']

def whoGoesFirst():
    # Randomly choose the player who goes first.
    if random.randint(0, 1) == 0:
        return 'computer'
    else:
        return 'player'

def playAgain():

    # This function returns True if the player wants to play again, otherwise it returns False.
    print('Do you want to play again? (yes or no)')
    return input().lower().startswith('y')

def makeMove(board, letter, move):
    board[move] = letter

def isWinner(bo, le):
    # Given a board and a player’s letter, this function returns True if that player has won.
    # We use bo instead of board and le instead of letter so we don’t have to type as much.

    return ((bo[7] == le and bo[8] == le and bo[9] == le) or # across the top
    (bo[4] == le and bo[5] == le and bo[6] == le) or # across the middle
    (bo[1] == le and bo[2] == le and bo[3] == le) or # across the bottom
    (bo[7] == le and bo[4] == le and bo[1] == le) or # down the left side
    (bo[8] == le and bo[5] == le and bo[2] == le) or # down the middle
    (bo[9] == le and bo[6] == le and bo[3] == le) or # down the right side
    (bo[7] == le and bo[5] == le and bo[3] == le) or # diagonal
    (bo[9] == le and bo[5] == le and bo[1] == le)) # diagonal

def getBoardCopy(board):
    # Make a duplicate of the board list and return it the duplicate.
    dupeBoard = []

    for i in board:
        dupeBoard.append(i)

    return dupeBoard

def isSpaceFree(board, move):
    # Return true if the passed move is free on the passed board.
    return board[move] == ' '

def getPlayerMove(board):
    # Let the player type in their move.
    move = ' '
    while move not in '1 2 3 4 5 6 7 8 9'.split() or not isSpaceFree(board, int(move)):
        print('What is your next move? (1-9)')
        move = input()
    return int(move)

def chooseRandomMoveFromList(board, movesList):
    # Returns a valid move from the passed list on the passed board.
    # Returns None if there is no valid move.
    possibleMoves = []

    for i in movesList:
        if isSpaceFree(board, i):
            possibleMoves.append(i)

    if len(possibleMoves) != 0:
        return random.choice(possibleMoves)
    else:
        return None

def getComputerMove(board, computerLetter):
    # Given a board and the computer's letter, determine where to move and return that move.
    if computerLetter == 'X':
        playerLetter = 'O'
    else:
        playerLetter = 'X'

    # Here is our algorithm for our Tic Tac Toe AI:
    # First, check if we can win in the next move
    for i in range(1, 10):
        copy = getBoardCopy(board)

        if isSpaceFree(copy, i):
            makeMove(copy, computerLetter, i)
            if isWinner(copy, computerLetter):
                return i

    # Check if the player could win on their next move, and block them.
    for i in range(1, 10):
        copy = getBoardCopy(board)
        if isSpaceFree(copy, i):
            makeMove(copy, playerLetter, i)
            if isWinner(copy, playerLetter):
                return i

    # Try to take one of the corners, if they are free.
    move = chooseRandomMoveFromList(board, [1, 3, 7, 9])
    if move != None:
        return move

    # Try to take the center, if it is free.
    if isSpaceFree(board, 5):
        return 5

    # Move on one of the sides.
    return chooseRandomMoveFromList(board, [2, 4, 6, 8])

def isBoardFull(board):
    # Return True if every space on the board has been taken. Otherwise return False.
    for i in range(1, 10):
        if isSpaceFree(board, i):
            return False
    return True

print('Welcome to Tic Tac Toe!')

while True:
    # Reset the board
    theBoard = [' '] * 10
    playerLetter, computerLetter = inputPlayerLetter()
    turn = whoGoesFirst()
    print('The ' + turn + ' will go first.')
    gameIsPlaying = True

    while gameIsPlaying:
        if turn == 'player':
            # Player’s turn.
            drawBoard(theBoard)
            move = getPlayerMove(theBoard)
            makeMove(theBoard, playerLetter, move)

            if isWinner(theBoard, playerLetter):
                drawBoard(theBoard)
                print('Hooray! You have won the game!')
                gameIsPlaying = False

            else:
                if isBoardFull(theBoard):
                    drawBoard(theBoard)
                    print('The game is a tie!')
                    break
                else:
                    turn = 'computer'
        else:
            # Computer’s turn.
            move = getComputerMove(theBoard, computerLetter)

            makeMove(theBoard, computerLetter, move)

            if isWinner(theBoard, computerLetter):
                drawBoard(theBoard)
                print('The computer has beaten you! You lose.')
                gameIsPlaying = False
            else:
                if isBoardFull(theBoard):
                    drawBoard(theBoard)
                    print('The game is a tie!')
                    break
                else:
                    turn = 'player'

    if not playAgain():
        break
#+end_src
* Functional Loops

Typically, imperative style loops aren't necessary in Haskell. Most of the time, loops can be replaced by an operation on a list.

| function      | Use case                                                  | example                                        |
|---------------+-----------------------------------------------------------+------------------------------------------------|
| foldr         | Calculate a value using every element                     | summing a list                                 |
| unfoldr       | Generate a list using a starting value                    | building up a range                            |
| map           | perform an operation on each element separately           | for loop to modify elements                    |
| forM_  mapM_  | perform an effectful operation on each element separately | for loop to print elements                     |
| filter        | remove elements from list which don't pass a test         | removing inactive users from list              |
| any           | see if any element passes a test                          | see if an element is in a list                 |
| all           | see if all elements pass a test                           | see if all test results are valid              |
| take          | operate only on the starting portion of a list            | drop all elements from a list after some index |
| drop          | operate only on the ending portion of a list              | traverse to the nth position of a list         |

#+BEGIN_SRC cpp
#include <iostream>

int main() {
  int sum = 0;
  for(int i = 1; i < 10; i++){
    sum += i;
  }
  std::cout << sum << "\n";
  return 0;
}
#+END_SRC

#+BEGIN_SRC haskell
main = print (foldr (+) 0 [1..9])
#+END_SRC

Loops as they are normally encountered in other languages can be imitated using functions from =Control.Monad=

#+BEGIN_SRC cpp
#include <iostream>

int main() {
  for(int i = 0; i < 10; i++) {
    std::cout << i << "\n";
  }
  return 0;
}
#+END_SRC

#+BEGIN_SRC haskell
import Control.Monad

main = do
  forM_ [0..9] $ \i -> do
    print i
#+END_SRC

Note, however, that the above and be simplified to the following...

#+BEGIN_SRC haskell
import Control.Monad

main = forM_ [0..9] print
#+END_SRC

More list operations can be found here:
https://hackage.haskell.org/package/base-4.12.0.0/docs/Data-List.html

More effective operations can be found here:
https://hackage.haskell.org/package/base-4.9.1.0/docs/Control-Monad.html

* Null references
  
Sometimes it is useful to operate on something like a pointer.

#+BEGIN_SRC java
class Program {
  static Integer getNthUser(int n, Integer[] users) {
    if(n < users.length) {
      return users[n];
    }
    else {
      return null;
    }
  }
  
  static void main(String[] args) {
    Integer[] numbers = [1,2,3,4,5,6,7];
    Integer x = getNthUser(4, numbers);
    if (x == null) {
      System.out.println("Could not pull number out!");
    } else {
      System.out.println("Got the number! It was " + x);
    }
  }
}
#+END_SRC

For these cases we can use the =Maybe= type to add an additional =Nothing= value to any type, emulating the possibility of a null value.

#+BEGIN_SRC haskell
import Data.Vector

getNthUser :: Int -> Vector User -> Maybe User
getNthUser n users =
  if n < length users
    then Just (users ! n)
    else Nothing

main = do
  let numbers = [1..7]
  case getNthUser 4 numbers of
    Nothing -> print "Could not pull number out!"
    Just x -> print ("Got the number! It was " <> show x)
#+END_SRC

