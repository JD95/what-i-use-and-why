#+title: Installing Emacs
#+startup: hidestar
#+startup: indent

# latex options
#+options: toc:nil num:nil html-postamble:nil date:nil
#+latex_header: \usepackage[margin=1.5in]{geometry}
#+latex_header: \usepackage{setspace}

* Windows
1. [[./installing-chocolatey.org][Install chocolatey]]
2. Use chocolatey to install emacs
#+BEGIN_SRC bash
choco install emacs64
#+END_SRC 
3. [[./installing-git.org][Install git]] 

* macOS

* Linux
